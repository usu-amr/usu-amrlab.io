const webpack = require('webpack')
const signale = require('signale')
const sass = require('sass')

const HtmlWebPackPlugin = require("html-webpack-plugin")
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin")
const TerserPlugin = require('terser-webpack-plugin')
const CopyPlugin = require('copy-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')

const path = require('path')
const context = path.resolve(__dirname, 'src')

const outputPath = 'public'
const baseEntry = './base.js'
const title = 'Aggie Marine Robotics'

const { Signale } = signale
const webpackSignale = new Signale({
  interactive: true, 
  scope: 'webpack'
})

const progressHandler = (percentage, message, ...args) => {
  if(percentage < 1){
    webpackSignale.await(`[${(percentage * 100).toFixed(0)}%] ${message}`)
  }else{
    webpackSignale.complete(`Webpack finished`)
  }
};

module.exports = (env, argv) => {
  signale.info(`Working in: ${context}`)
  
  const isProduction = argv.mode != 'development'

  signale.info(`Building production bundle: ${isProduction ? 'Yes' : 'No'}`)
  if(isProduction){
    signale.info(`Bundle will not include source maps`)
    signale.info(`Bundle will be minified`)
    signale.info(`CSS will be pulled out into separate files`)
  }

  webpackSignale.await(`Webpack starting...`)
  return generateConfig({context, isProduction, baseEntry, outputPath})
}

function generateConfig({ context, isProduction, baseEntry, outputPath }){
  const cssModuleScopedNames = isProduction ? '[hash:base64:10]' : '[path]__[name]__[local]'

  return {
    context: context,
    devtool: isProduction ? false : 'source-map',
    entry: {
      base: baseEntry
    },
    output: {
      path: path.resolve(__dirname, outputPath),
      filename: isProduction ? '[id].[contenthash].js' : '[name].js',
    },
    optimization: {
      minimize: isProduction,
      minimizer: isProduction ? [
        new TerserPlugin({
          sourceMap: false,
          cache: true,
          parallel: true,
          extractComments: true,
        }),
        new OptimizeCSSAssetsPlugin({}),
      ] : [],
      splitChunks: isProduction ? {
        chunks: 'all',
        // cacheGroups: {
        //   styles: {
        //     name: 'styles',
        //     test: /\.(scss|sass|css)$/,
        //     chunks: 'all',
        //     enforce: true
        //   }
        // }
      } : false,
    },
    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          exclude: /node_modules/,
          use: {
            loader: "babel-loader",
            options: {
              presets: [
                ["@babel/preset-env", {
                  modules: false,
                }],
                "@babel/preset-react",
              ],
              plugins: [
                "@babel/plugin-syntax-dynamic-import",
                [
                  'react-css-modules',
                  {
                    context,
                    generateScopedName: cssModuleScopedNames,
                    filetypes: {
                      '.scss': {
                        syntax: 'postcss-scss'
                      }
                    }
                  }
                ]
              ]
            },
          }
        },
        {
          test: /\.hbs$/,
          loader: 'handlebars-loader'
        },
        {
          oneOf: [
            {
              test: /\.(css|scss|sass)$/,
              resourceQuery: /^\?raw$/,
              use: [
                {
                  loader: isProduction ? MiniCssExtractPlugin.loader : 'style-loader',
                },
                {
                  loader: 'css-loader',
                  options: {
                    modules: false,
                    sourceMap: !isProduction,
                  }
                },
                {
                  loader: "sass-loader",
                  options: {
                    implementation: sass,
                    sourceMap: !isProduction,
                  }
                }
              ]
            },
            {
              test: /\.(css|scss|sass)$/,
              use: [
                {
                  loader: isProduction ? MiniCssExtractPlugin.loader : 'style-loader',
                },
                {
                  loader: 'css-loader',
                  options: {
                    url: false,
                    modules: true,
                    localIdentName: cssModuleScopedNames,
                    sourceMap: !isProduction,
                  }
                },
                {
                  loader: "sass-loader",
                  options: {
                    implementation: sass,
                    sourceMap: !isProduction,
                  }
                }
              ]
            }
          ]
        }
      ]
    },
    plugins: isProduction ? [
      new CleanWebpackPlugin(),
      new HtmlWebPackPlugin({
        template: "./index.hbs",
        filename: "./index.html",
        minify: {
          collapseWhitespace: true,
          removeComments: true,
          removeRedundantAttributes: true,
          removeScriptTypeAttributes: true,
          removeStyleLinkTypeAttributes: true,
          useShortDoctype: true
        },
        showErrors: false,
        templateParameters: {
          title: title,
        }
      }),
      new webpack.ProvidePlugin({
        "React": "react",
      }),
      new MiniCssExtractPlugin({
        filename: "[id].[contenthash].css",
        chunkFilename: "[id].[contenthash].css"
      }),
      new CopyPlugin([
        { from: '../assets', to: 'assets' },
        { from: '../assets/favicon.ico', to: 'favicon.ico' },
      ]),
    ] : [
      new webpack.ProgressPlugin(progressHandler),
      new HtmlWebPackPlugin({
        template: "./index.hbs",
        filename: "./index.html",
        minify: false,
        showErrors: true,
        templateParameters: {
          title: title,
        }
      }),
      new webpack.ProvidePlugin({
        "React": "react",
      }),
      new CopyPlugin([
        { from: '../assets', to: 'assets' },
        { from: '../assets/favicon.ico', to: 'favicon.ico' },
      ]),
    ]
  }
}
