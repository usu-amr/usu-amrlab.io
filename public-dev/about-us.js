(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["about-us"],{

/***/ "../node_modules/css-loader/dist/cjs.js?!../node_modules/sass-loader/lib/loader.js?!./components/container/about-us/index.scss":
/*!***********************************************************************************************************************************************************************!*\
  !*** ../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-1-1!../node_modules/sass-loader/lib/loader.js??ref--6-oneOf-1-2!./components/container/about-us/index.scss ***!
  \***********************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/api.js */ "../node_modules/css-loader/dist/runtime/api.js")(true);
// Module
exports.push([module.i, ".components-container-about-us-__index__hero {\n  overflow: hidden;\n  min-height: 20rem;\n}\n\n.components-container-about-us-__index__hero-img {\n  width: 100%;\n  height: 100%;\n  object-fit: cover;\n}\n\n.components-container-about-us-__index__content {\n  margin: 2rem;\n}", "",{"version":3,"sources":["/mnt/c/appdev/amr/usu-amr.gitlab.io/src/components/container/about-us/index.scss","index.scss"],"names":[],"mappings":"AAAA;EACE,gBAAA;EACA,iBAAA;ACCF;;ADEA;EACE,WAAA;EACA,YAAA;EACA,iBAAA;ACCF;;ADEA;EACE,YAAA;ACCF","file":"index.scss","sourcesContent":[".hero {\r\n  overflow: hidden;\r\n  min-height: 20rem;\r\n}\r\n\r\n.hero-img {\r\n  width: 100%;\r\n  height: 100%;\r\n  object-fit: cover;\r\n}\r\n\r\n.content {\r\n  margin: 2rem;\r\n}",".hero {\n  overflow: hidden;\n  min-height: 20rem;\n}\n\n.hero-img {\n  width: 100%;\n  height: 100%;\n  object-fit: cover;\n}\n\n.content {\n  margin: 2rem;\n}"]}]);

// Exports
exports.locals = {
	"hero": "components-container-about-us-__index__hero",
	"hero-img": "components-container-about-us-__index__hero-img",
	"content": "components-container-about-us-__index__content"
};

/***/ }),

/***/ "./components/container/about-us/index.js":
/*!************************************************!*\
  !*** ./components/container/about-us/index.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(React) {/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "../node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _index_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./index.scss */ "./components/container/about-us/index.scss");
/* harmony import */ var _index_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_index_scss__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _presentational_basic_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../presentational/basic-page */ "./components/presentational/basic-page/index.js");
/* harmony import */ var bloomer__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! bloomer */ "../node_modules/bloomer/bundles/bloomer.min.js");
/* harmony import */ var bloomer__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(bloomer__WEBPACK_IMPORTED_MODULE_3__);





var AboutUs = function AboutUs() {
  return React.createElement(_presentational_basic_page__WEBPACK_IMPORTED_MODULE_2__["FillParent"], {
    forceFill: true,
    minHeight: "100%"
  }, React.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null, React.createElement(_presentational_basic_page__WEBPACK_IMPORTED_MODULE_2__["Hero"], {
    grow: true,
    className: "components-container-about-us-__index__hero"
  }, React.createElement("img", {
    src: "https://dummyimage.com/1920x1080/009ddc/eee",
    className: "components-container-about-us-__index__hero-img"
  })), React.createElement(_presentational_basic_page__WEBPACK_IMPORTED_MODULE_2__["Content"], null, React.createElement(bloomer__WEBPACK_IMPORTED_MODULE_3__["Content"], {
    className: "components-container-about-us-__index__content"
  }, React.createElement("h1", null, "About Us"), React.createElement("p", null, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed quis enim est. Nulla facilisi. Proin eget velit et nisi scelerisque convallis vitae id est. Ut imperdiet vestibulum magna, at imperdiet ante facilisis ac. Quisque eleifend urna dui, vel elementum ligula auctor at. Mauris fermentum diam risus, sit amet egestas leo varius vitae. Aliquam placerat sapien ac facilisis dapibus. Proin sed rutrum augue. Vivamus blandit dui vitae posuere sodales. Donec id ligula ut libero dapibus condimentum quis vitae tellus. Pellentesque tempor purus et urna gravida, sit amet aliquam urna tincidunt. Nunc hendrerit, lacus sollicitudin hendrerit hendrerit, turpis risus molestie purus, nec imperdiet turpis est eu tellus. Quisque sed urna eu massa finibus cursus. Nullam feugiat diam et dolor convallis, et dictum est vulputate.")))));
};

/* harmony default export */ __webpack_exports__["default"] = (AboutUs);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! react */ "../node_modules/react/index.js")))

/***/ }),

/***/ "./components/container/about-us/index.scss":
/*!**************************************************!*\
  !*** ./components/container/about-us/index.scss ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-1-1!../../../../node_modules/sass-loader/lib/loader.js??ref--6-oneOf-1-2!./index.scss */ "../node_modules/css-loader/dist/cjs.js?!../node_modules/sass-loader/lib/loader.js?!./components/container/about-us/index.scss");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "../node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ })

}]);
//# sourceMappingURL=about-us.js.map