(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["meet-the-team"],{

/***/ "../node_modules/css-loader/dist/cjs.js?!../node_modules/sass-loader/lib/loader.js?!./components/container/meet-the-team/index.scss":
/*!****************************************************************************************************************************************************************************!*\
  !*** ../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-1-1!../node_modules/sass-loader/lib/loader.js??ref--6-oneOf-1-2!./components/container/meet-the-team/index.scss ***!
  \****************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/api.js */ "../node_modules/css-loader/dist/runtime/api.js")(true);
// Module
exports.push([module.i, ".components-container-meet-the-team-__index__content {\n  margin: 2rem;\n}\n\n.components-container-meet-the-team-__index__team-image {\n  max-height: 30rem;\n}", "",{"version":3,"sources":["/mnt/c/appdev/amr/usu-amr.gitlab.io/src/components/container/meet-the-team/index.scss","index.scss"],"names":[],"mappings":"AAAA;EACE,YAAA;ACCF;;ADEA;EACE,iBAAA;ACCF","file":"index.scss","sourcesContent":[".content{\r\n  margin: 2rem;\r\n}\r\n\r\n.team-image{\r\n  max-height: 30rem;\r\n}",".content {\n  margin: 2rem;\n}\n\n.team-image {\n  max-height: 30rem;\n}"]}]);

// Exports
exports.locals = {
	"content": "components-container-meet-the-team-__index__content",
	"team-image": "components-container-meet-the-team-__index__team-image"
};

/***/ }),

/***/ "./components/container/meet-the-team/index.js":
/*!*****************************************************!*\
  !*** ./components/container/meet-the-team/index.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(React) {/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "../node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _index_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./index.scss */ "./components/container/meet-the-team/index.scss");
/* harmony import */ var _index_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_index_scss__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _presentational_basic_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../presentational/basic-page */ "./components/presentational/basic-page/index.js");
/* harmony import */ var bloomer__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! bloomer */ "../node_modules/bloomer/bundles/bloomer.min.js");
/* harmony import */ var bloomer__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(bloomer__WEBPACK_IMPORTED_MODULE_3__);





var MeetTheTeam = function MeetTheTeam() {
  return React.createElement(_presentational_basic_page__WEBPACK_IMPORTED_MODULE_2__["FillParent"], null, React.createElement(_presentational_basic_page__WEBPACK_IMPORTED_MODULE_2__["Content"], null, React.createElement(bloomer__WEBPACK_IMPORTED_MODULE_3__["Container"], null, React.createElement(bloomer__WEBPACK_IMPORTED_MODULE_3__["Content"], {
    className: "components-container-meet-the-team-__index__content"
  }, React.createElement("h2", null, "Aggie Marine Robotics Team 2018 - 2019"), React.createElement("img", {
    src: "https://dummyimage.com/1280x800/00a362/ffffff",
    className: "components-container-meet-the-team-__index__team-image"
  }), React.createElement("p", null, "footer with names of members and not pictured"), React.createElement("h2", null, "2018 - 2019 Team Leads"), React.createElement("p", null, React.createElement("strong", null, "Rachel Wall"), " - Team lead"), React.createElement("p", null, React.createElement("strong", null, "Reed Thurber"), " - Vice lead"), React.createElement("p", null, React.createElement("strong", null, "Jacie Grow"), " - Mechanical Captain"), React.createElement("p", null, React.createElement("strong", null, "Bryant Casutt"), " - Electrical Captain"), React.createElement("p", null, React.createElement("strong", null, "Taylor Jones"), " - Software Captain")))));
};

/* harmony default export */ __webpack_exports__["default"] = (MeetTheTeam);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! react */ "../node_modules/react/index.js")))

/***/ }),

/***/ "./components/container/meet-the-team/index.scss":
/*!*******************************************************!*\
  !*** ./components/container/meet-the-team/index.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-1-1!../../../../node_modules/sass-loader/lib/loader.js??ref--6-oneOf-1-2!./index.scss */ "../node_modules/css-loader/dist/cjs.js?!../node_modules/sass-loader/lib/loader.js?!./components/container/meet-the-team/index.scss");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "../node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ })

}]);
//# sourceMappingURL=meet-the-team.js.map