(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["homepage"],{

/***/ "../node_modules/css-loader/dist/cjs.js?!../node_modules/sass-loader/lib/loader.js?!./components/container/homepage/index.scss":
/*!***********************************************************************************************************************************************************************!*\
  !*** ../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-1-1!../node_modules/sass-loader/lib/loader.js??ref--6-oneOf-1-2!./components/container/homepage/index.scss ***!
  \***********************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/api.js */ "../node_modules/css-loader/dist/runtime/api.js")(true);
// Module
exports.push([module.i, ".components-container-homepage-__index__hero {\n  overflow: hidden;\n  min-height: 20rem;\n  position: relative;\n}\n\n.components-container-homepage-__index__hero-image {\n  position: absolute;\n  left: 0px;\n  right: 0px;\n  top: 0px;\n  bottom: 0px;\n  width: 100%;\n  height: 100%;\n  object-fit: cover;\n  z-index: -10;\n}\n\n.components-container-homepage-__index__hero-image-cover {\n  position: absolute;\n  left: 0px;\n  right: 0px;\n  top: 0px;\n  bottom: 0px;\n  width: 100%;\n  height: 100%;\n  z-index: -10;\n  opacity: 0.6;\n  background: #06246b;\n}\n\n.components-container-homepage-__index__hero-content {\n  z-index: 10;\n  height: 100%;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  text-align: center;\n}\n.components-container-homepage-__index__hero-content h1 {\n  color: white;\n  font-size: 2.5em;\n  margin-bottom: 0.6666em;\n}\n.components-container-homepage-__index__hero-content h2 {\n  color: white;\n  font-size: 1.2em;\n  margin-bottom: 0.6666em;\n}\n.components-container-homepage-__index__hero-content a, .components-container-homepage-__index__hero-content button {\n  margin: 0.5rem 1rem;\n}\n\n.components-container-homepage-__index__above-fold-content {\n  margin: 2rem;\n  margin-bottom: 2rem;\n}\n\n.components-container-homepage-__index__below-fold-content {\n  margin-top: 0px;\n  margin: 2rem;\n}", "",{"version":3,"sources":["/mnt/c/appdev/amr/usu-amr.gitlab.io/src/components/container/homepage/index.scss","index.scss"],"names":[],"mappings":"AAAA;EACE,gBAAA;EACA,iBAAA;EACA,kBAAA;ACCF;;ADEA;EACE,kBAAA;EACA,SAAA;EACA,UAAA;EACA,QAAA;EACA,WAAA;EACA,WAAA;EACA,YAAA;EACA,iBAAA;EACA,YAAA;ACCF;;ADEA;EACE,kBAAA;EACA,SAAA;EACA,UAAA;EACA,QAAA;EACA,WAAA;EACA,WAAA;EACA,YAAA;EACA,YAAA;EACA,YAAA;EACA,mBAAA;ACCF;;ADEA;EACE,WAAA;EACA,YAAA;EACA,aAAA;EACA,sBAAA;EACA,uBAAA;EACA,kBAAA;ACCF;ADCE;EACE,YAAA;EACA,gBAAA;EACA,uBAAA;ACCJ;ADEE;EACE,YAAA;EACA,gBAAA;EACA,uBAAA;ACAJ;ADGE;EACE,mBAAA;ACDJ;;ADKA;EACE,YAAA;EACA,mBAAA;ACFF;;ADKA;EACE,eAAA;EACA,YAAA;ACFF","file":"index.scss","sourcesContent":[".hero {\r\n  overflow: hidden;\r\n  min-height: 20rem;\r\n  position: relative;\r\n}\r\n\r\n.hero-image {\r\n  position: absolute;\r\n  left: 0px;\r\n  right: 0px;\r\n  top: 0px;\r\n  bottom: 0px;\r\n  width: 100%;\r\n  height: 100%;\r\n  object-fit: cover;\r\n  z-index: -10;\r\n}\r\n\r\n.hero-image-cover {\r\n  position: absolute;\r\n  left: 0px;\r\n  right: 0px;\r\n  top: 0px;\r\n  bottom: 0px;\r\n  width: 100%;\r\n  height: 100%;\r\n  z-index: -10;\r\n  opacity: 0.6;\r\n  background: #06246b;\r\n}\r\n\r\n.hero-content {\r\n  z-index: 10;\r\n  height: 100%;\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content: center;\r\n  text-align: center;\r\n\r\n  h1{\r\n    color: white;\r\n    font-size: 2.5em;\r\n    margin-bottom: 0.6666em;\r\n  }\r\n\r\n  h2{\r\n    color: white;\r\n    font-size: 1.2em;\r\n    margin-bottom: 0.6666em;\r\n  }\r\n\r\n  a, button{\r\n    margin: 0.5rem 1rem;\r\n  }\r\n}\r\n\r\n.above-fold-content{\r\n  margin: 2rem;\r\n  margin-bottom: 2rem;\r\n}\r\n\r\n.below-fold-content{\r\n  margin-top: 0px;\r\n  margin: 2rem;\r\n}\r\n",".hero {\n  overflow: hidden;\n  min-height: 20rem;\n  position: relative;\n}\n\n.hero-image {\n  position: absolute;\n  left: 0px;\n  right: 0px;\n  top: 0px;\n  bottom: 0px;\n  width: 100%;\n  height: 100%;\n  object-fit: cover;\n  z-index: -10;\n}\n\n.hero-image-cover {\n  position: absolute;\n  left: 0px;\n  right: 0px;\n  top: 0px;\n  bottom: 0px;\n  width: 100%;\n  height: 100%;\n  z-index: -10;\n  opacity: 0.6;\n  background: #06246b;\n}\n\n.hero-content {\n  z-index: 10;\n  height: 100%;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  text-align: center;\n}\n.hero-content h1 {\n  color: white;\n  font-size: 2.5em;\n  margin-bottom: 0.6666em;\n}\n.hero-content h2 {\n  color: white;\n  font-size: 1.2em;\n  margin-bottom: 0.6666em;\n}\n.hero-content a, .hero-content button {\n  margin: 0.5rem 1rem;\n}\n\n.above-fold-content {\n  margin: 2rem;\n  margin-bottom: 2rem;\n}\n\n.below-fold-content {\n  margin-top: 0px;\n  margin: 2rem;\n}"]}]);

// Exports
exports.locals = {
	"hero": "components-container-homepage-__index__hero",
	"hero-image": "components-container-homepage-__index__hero-image",
	"hero-image-cover": "components-container-homepage-__index__hero-image-cover",
	"hero-content": "components-container-homepage-__index__hero-content",
	"above-fold-content": "components-container-homepage-__index__above-fold-content",
	"below-fold-content": "components-container-homepage-__index__below-fold-content"
};

/***/ }),

/***/ "./components/container/homepage/index.js":
/*!************************************************!*\
  !*** ./components/container/homepage/index.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(React) {/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "../node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _index_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./index.scss */ "./components/container/homepage/index.scss");
/* harmony import */ var _index_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_index_scss__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _presentational_basic_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../presentational/basic-page */ "./components/presentational/basic-page/index.js");
/* harmony import */ var bloomer__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! bloomer */ "../node_modules/bloomer/bundles/bloomer.min.js");
/* harmony import */ var bloomer__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(bloomer__WEBPACK_IMPORTED_MODULE_3__);





var HomePage = function HomePage() {
  return React.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null, React.createElement(_presentational_basic_page__WEBPACK_IMPORTED_MODULE_2__["FillParent"], {
    forceFill: true,
    minHeight: "100%"
  }, React.createElement(_presentational_basic_page__WEBPACK_IMPORTED_MODULE_2__["Hero"], {
    grow: true,
    className: "components-container-homepage-__index__hero"
  }, React.createElement("img", {
    src: "assets/water4.jpg",
    className: "components-container-homepage-__index__hero-image"
  }), React.createElement("div", {
    className: "components-container-homepage-__index__hero-image-cover"
  }), React.createElement("div", {
    className: "components-container-homepage-__index__hero-content"
  }, React.createElement("h1", null, "Aggie Marine Robotics"), React.createElement("h2", null, "Complete Underwater Autonomy"), React.createElement(bloomer__WEBPACK_IMPORTED_MODULE_3__["Container"], null, React.createElement(bloomer__WEBPACK_IMPORTED_MODULE_3__["Button"], {
    href: "#/join",
    isColor: "primary"
  }, "Join the Team"), React.createElement(bloomer__WEBPACK_IMPORTED_MODULE_3__["Button"], {
    href: "#/sponsor",
    isColor: "primary"
  }, "Sponsor Us")))), React.createElement(_presentational_basic_page__WEBPACK_IMPORTED_MODULE_2__["Content"], null, React.createElement(bloomer__WEBPACK_IMPORTED_MODULE_3__["Container"], null, React.createElement(bloomer__WEBPACK_IMPORTED_MODULE_3__["Content"], {
    className: "components-container-homepage-__index__above-fold-content"
  }, React.createElement("h3", null, "Who We Are"), React.createElement("p", null, "Aggie Marine Robotics is a multidisciplinary undergraduate engineering team at Utah State University dedicated to creating advanced autonomous underwater vehicles."), React.createElement("h3", null, "Our Mission"), React.createElement("p", null, "To successfully design, build, and deploy mission ready AUV's for the AUVSI Robosub competition."))))), React.createElement(_presentational_basic_page__WEBPACK_IMPORTED_MODULE_2__["Content"], null, React.createElement(bloomer__WEBPACK_IMPORTED_MODULE_3__["Container"], null, React.createElement(bloomer__WEBPACK_IMPORTED_MODULE_3__["Content"], {
    className: "components-container-homepage-__index__below-fold-content"
  }, React.createElement("h3", null, "The Competition"), React.createElement("p", null, "The AUVSI RoboSub competition is an international competition held every Summer in San Diego. To learn more about the competition check out the official ", React.createElement("a", {
    href: "https://www.robonation.org/competition/robosub"
  }, "RoboSub Website"), "."), React.createElement("h3", null, "Meet the team"), React.createElement("h3", null, "Meet the subs"), React.createElement("h3", null, "Aggie Marine Robotics Would like to thank our sponsors")))));
};

/* harmony default export */ __webpack_exports__["default"] = (HomePage);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! react */ "../node_modules/react/index.js")))

/***/ }),

/***/ "./components/container/homepage/index.scss":
/*!**************************************************!*\
  !*** ./components/container/homepage/index.scss ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-1-1!../../../../node_modules/sass-loader/lib/loader.js??ref--6-oneOf-1-2!./index.scss */ "../node_modules/css-loader/dist/cjs.js?!../node_modules/sass-loader/lib/loader.js?!./components/container/homepage/index.scss");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "../node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ })

}]);
//# sourceMappingURL=homepage.js.map