import { render } from 'react-dom'
import App from './app'

import './base.raw.scss?raw'

const root = document.getElementById("root")

render(
  <App/>
, root)
