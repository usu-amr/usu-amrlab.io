import './page.scss'

const Page = ({ children }) =>
  <div styleName='page'>
    {children}
  </div>

export default Page