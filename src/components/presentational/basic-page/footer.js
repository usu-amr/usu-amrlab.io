import './footer.scss'

const Footer = ({ children, className }) =>
  <footer styleName='footer' className={className}>
    {children}
  </footer>

export default Footer