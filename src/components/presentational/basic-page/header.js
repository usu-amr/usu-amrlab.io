import './header.scss'
import Headroom from 'react-headroom'

const Header = ({ children, floating=false }) => {
  if(floating){
    return <Headroom disableInlineStyles>
      {children}
    </Headroom>
  }else{
    return <header styleName='header'>
      {children}
    </header>
  }
}

export default Header