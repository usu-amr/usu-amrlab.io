import './fill-parent.scss'

const FillParent = ({ children, forceFill=false, minHeight=null }) =>
  <div
    styleName={forceFill ? 'fill-parent-force' : 'fill-parent'}
    style={minHeight != null ? {minHeight} : null}
  >
    {children}
  </div>

export default FillParent