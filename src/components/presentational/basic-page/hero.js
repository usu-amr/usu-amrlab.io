import './hero.scss'

const Hero = ({ children, grow=false, className }) =>
  <div styleName={grow ? 'hero-grow' : 'hero-normal'} className={className}>
    {children}
  </div>

export default Hero