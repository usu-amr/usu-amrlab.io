import './scroll.scss'

const Scroll = ({ children, useBody }) =>
  <div styleName={useBody ? 'scroll scroll' : 'scroll scroll-self'}>
    {children}
  </div>

export default Scroll