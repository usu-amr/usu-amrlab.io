import './content.scss'

const Content = ({ children, grow=false }) =>
  <div styleName={grow ? 'content-grow' : 'content-normal'}>
    {children}
  </div>

export default Content