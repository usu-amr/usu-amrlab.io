import './index.scss'
import { 
  Notification
} from 'bloomer'

const DevWarning = () => 
  <Notification isColor='warning' styleName="warning">
    Notice: This is a development build of this website. Information may be incorrect or missing.
  </Notification>

export default DevWarning