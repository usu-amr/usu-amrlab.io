import { Fragment } from 'react'
import './index.scss'
import { 
  FillParent, Hero, Content, Footer
} from '../../presentational/basic-page'
import { Content as BloomerContent, Container, Table } from 'bloomer'

const Auvs = () => 
  <FillParent forceFill={true} minHeight='100%'>
    <Content>
      <Container>
        <BloomerContent styleName='content'>
          <h1>Our AUV's</h1>
          <h2>Next Sub</h2>


          <h2>Seahorse Feb 2018 - Now</h2>
          <img src="assets/tijar.png" styleName='image'></img>
          <p>
            The direct descendent of T.I.J.A.R., Seahorse 
            was built with a focus on agility and 
            manuverability, featuring a smaller, lighter 
            body, compact electronics, and a variety of 
            new sensors, Seahorse is is outfitted to not 
            only qualify, but to roll, strafe, and spin 
            like no AMR sub before it. The perfect 
            platform to develop our autonomous control 
            software, Seahorse is our most capable AUV yet.
          </p>

          <h2>T.I.J.A.R Oct 2018 - Feb 2019</h2>
          <img src="assets/tijar_original.png" styleName='image'></img>
          <p>
            The first sub of the 2018 - 2019 year, T.I.J.A.R., 
            or This is Just a Rig, is the beginning of 
            the design process for the next competition 
            sub. Built quickly with a focus on testing 
            and experimentation, the sub features a 
            modular t-slot aluminum frame, a spacious 
            electronics hull, and the teams first eight 
            thruster motor layout. The sub is the perfect 
            prototype AUV to test the first phase of 
            hardware, electronics, and software.
          </p>

          <h2>Poseidon I - II Dec 2015 - Jul 2018</h2>
          <img src="assets/pogen2.png" styleName='image'></img>
          <p>
            The previous generation of USU robosub vehicles, 
            the Poseidon models were built with a focus on 
            experimental software development first, 
            utilizing simple robust mechanical designs 
            and modular electronics. Featuring
          </p>

          <h1>Land Bots</h1>
          <h2>The AMR Rover</h2>
          <img src="assets/omni.png" styleName='image'></img>
          <p>
            The teams small, lightweight land cruiser. 
            With four omni-wheels the AMR Rover can 
            emulate all the same linear degrees of 
            motion as our subs except dive. This paired 
            with a front facing webcam makes the AMR 
            Rover a perfect on-land mini-sub for 
            debugging and testing software.
            <br/>
            "fun at events and fun to learn on"
            <br/>
            a fun addition to our land bot fleet
          </p>

          <h2>The AMR Tank</h2>
          <img src="assets/tank.png" styleName='image'></img>
          <p>
            Like the Rover
          </p>
          <ul>
            <li>good to learn electronics on</li>
            <li>fun at parties</li>
            <li>webcam allows us to use vision recognition</li>
          </ul>
        </BloomerContent>
      </Container>
    </Content>
  </FillParent>

export default Auvs