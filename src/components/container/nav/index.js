import { Component } from 'react'
import { 
  Navbar, NavbarBrand, NavbarItem, Icon, 
  NavbarLink, NavbarDivider, NavbarBurger,
  NavbarMenu, NavbarStart, NavbarDropdown, NavbarEnd,
  Field, Control, Button
} from 'bloomer'

class Nav extends Component{
  constructor(){
    super()

    this.state = {
      isMenuOpen: false,
      isContactMenuOpen: false
    }
  }

  handleBurgerClick(e){
    e.preventDefault()
    this.setState({
      isMenuOpen: !this.state.isMenuOpen
    })
  }

  handleNavClick(){
    this.setState({
      isMenuOpen: !this.state.isMenuOpen,
      isContactMenuOpen: false
    })
  }

  handleContactDropdown(e){
    e.preventDefault()
    e.stopPropagation()
    this.setState({
      isContactMenuOpen: !this.state.isContactMenuOpen
    })
  }

  render(){
    const { isMenuOpen, isContactMenuOpen } = this.state

    return <Navbar>
      <NavbarBrand>
        <NavbarItem>
          <img 
            src={'https://dummyimage.com/128x32/13293d/eee&text=AMR'} 
            style={{ marginRight: 20 }} 
          />
          Aggie Marine Robotics
        </NavbarItem>
        <NavbarBurger 
          isActive={isMenuOpen} 
          onClick={(e) => this.handleBurgerClick(e)} 
        />
      </NavbarBrand>
      <NavbarMenu 
        isActive={isMenuOpen} 
        onClick={() => this.handleNavClick()}
      >
        <NavbarEnd>
          <NavbarItem href='#/'>About Us</NavbarItem>
          <NavbarItem href='#/team'>The Team</NavbarItem>
          <NavbarItem href='#/submarines'>Submarines</NavbarItem>
          <NavbarItem href='#/join'>Join</NavbarItem>
          <NavbarItem href='#/sponsors'>Sponsors</NavbarItem>
          <NavbarItem hasDropdown isActive={isContactMenuOpen}>
            <NavbarLink onClick={(e) => this.handleContactDropdown(e)}>Contact Us</NavbarLink>
            <NavbarDropdown className='is-right'>
              <NavbarItem href='#/sponsor'>Become a Sponsor</NavbarItem>
              <NavbarItem href='#/contact'>Send Us a Message</NavbarItem>
            </NavbarDropdown>
          </NavbarItem>
        </NavbarEnd>
      </NavbarMenu>
    </Navbar>
  }
}

export default Nav