import { Fragment } from 'react'
import './index.scss'
import { 
  FillParent, Hero, Content, Footer
} from '../../presentational/basic-page'
import { 
  Content as BloomerContent, Section, Button, Container, 
  Field, Label, Control, FieldLabel, FieldBody, Input, Icon
} from 'bloomer'

const Sponsor = () => 
  <FillParent>
    <Content>
      <Container>
        <BloomerContent styleName='content'>
          <h2>Sponsor The Aggie Marine Robotics Team</h2>
          <p>If you would like to sponsor our team please contact us at <a href="mailto:aggiemarinerobotics@gmail.com">aggiemarinerobotics@gmail.com</a></p>
          <h3>What are you contributing to?</h3>
          <p>
          With your help we will be able to go to the 2019 competition and score
          more points than ever recorded in our team’s history. All of our members
          have been working on a completely new design this year and we are proud
          to say that we are already capable of pre-qualifying for this years
          competition.
          </p>
          <p>
          Our redesign has included an entirely new mechanical structure that
          supports eight thrusters enabling our AUV to rotate in any direction. Our
          new electronics system has been completely redesigned, enabling us to
          regulate all the vehicles power and communicate with a variety of sensors.
          Utilizing The Robot Operating System (ROS) our software team has
          created a virtual environment for teaching our submarine movement and
          manipulation. With our new processing capabilities the team is now
          working on developing computer vision for use in this year’s competition.
          </p>
          <h3>Sponsor visibility</h3>
          <p>
          Partnering with AMR will provide your
          organization visibility and promotion for
          your products and services in the AUV
          community. Each year our team participates
          in the AUVSI RoboSub competition, where
          your name and logo will be displayed at our
          team tent, on our uniforms and for Silver and
          Gold sponsors, on our submarine. 
          </p>
          <p>The competition has constant media coverage as
          well as exposure to 60 or more international
          teams of students. We also participate in a
          variety of community events such as the
          USU College of Engineering community
          night, outreach events to schools in Logan
          UT, and many other outreach events for
          students at USU.
          </p>


          <h3>Sponsorship opportunities</h3>
          <p>
          Your company can help our team in a
          variety of ways, a monetary donation,
          hardware donation, software donation,
          or by offering your technical assistance.
          Members of Aggie Marine Robotics are
          available to meet with you in person, by
          email, or over the phone to talk about
          our AUV and how your company can
          become a valuable partner to our team.
          </p>

          <h3>Where is your money going?</h3>
          <ul>
            <li>Acquisition of new hardware for the vehicle body</li>
            <li>Purchasing new sensors to measure velocity, depth, imagery, and sound</li>
            <li>Custom printed circuit boards</li>
            <li>Development of digital vision system and virtual test environment</li>
            <li>Underwater testing at local pools</li>
            <li>Travel to the AUVSI RoboSub competition</li>
          </ul>
        </BloomerContent>
      </Container>
    </Content>
  </FillParent>

export default Sponsor