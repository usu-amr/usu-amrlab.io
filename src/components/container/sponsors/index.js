import { Fragment } from 'react'
import './index.scss'
import { 
  FillParent, Hero, Content, Footer
} from '../../presentational/basic-page'
import { 
  Content as BloomerContent, Section, Button, Container, 
  Field, Label, Control, FieldLabel, FieldBody, Input, Icon
} from 'bloomer'

const Sponsor = () => 
  <FillParent>
    <Content>
      <Container>
        <BloomerContent styleName='content'>
          <h2>Sponsors of The Aggie Marine Robotics Team</h2>
          <ul>
            <li>Utah State University - Computer Science</li>
            <li>Utah State University - Electrical &amp; Computer Engineering</li>
            <li>Utah State University - Mechanical Engineering</li>
            <li>Utah State University - Space Dynamics Laboratory</li>
          </ul>
        </BloomerContent>
      </Container>
    </Content>
  </FillParent>

export default Sponsor