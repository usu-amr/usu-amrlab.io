import { Fragment } from 'react'
import './index.scss'
import { 
  FillParent, Hero, Content, Footer
} from '../../presentational/basic-page'
import { 
  Content as BloomerContent, Section, Button, Container, 
  Field, Label, Control, TextArea, FieldLabel, FieldBody, Input, Icon
} from 'bloomer'

const Contact = () => 
  <FillParent>
    <Content>
      <Container>
        <h2>Contact The Aggie Marine Robotics Team</h2>
        <form action="http://formspree.io/aggiemarinerobotics@gmail.com">
          <Field>
            <Label>Email</Label>
            <Control>
                <Input type="email" placeholder='Email Address' name="_replyto" />
            </Control>
          </Field>
          <Field>
              <Label>Message</Label>
              <Control>
                  <TextArea name="Body" placeholder={'Message Here'} />
              </Control>
          </Field>
          <Field isGrouped>
              <Control>
                  <Button isColor='primary'>Send</Button>
              </Control>
          </Field>
        </form>
      </Container>
    </Content>
  </FillParent>

export default Contact