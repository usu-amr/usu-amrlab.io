import { Fragment } from 'react'
import './index.scss'
import { 
  FillParent, Hero, Content, Footer
} from '../../presentational/basic-page'
import { 
  Content as BloomerContent, Section, Button, Container, 
  Field, Label, Control, FieldLabel, FieldBody, Input, Icon
} from 'bloomer'

const HomePage = () => 
  <Fragment>
    <FillParent forceFill={true} minHeight='100%'>
      <Hero grow={true} styleName='hero'>
        <img 
          src="assets/water4.jpg"
          styleName='hero-image'
        />
        <div styleName='hero-image-cover'></div>
        <div styleName="hero-content">
          <h1>Aggie Marine Robotics</h1>
          <h2>Complete Underwater Autonomy</h2>

            <Container>
              <Button href="#/join" isColor='primary'>Join the Team</Button>
              <Button href="#/sponsor" isColor='primary'>Sponsor Us</Button>
            </Container>
        </div>
      </Hero>
      <Content>
        <Container>
          <BloomerContent styleName='above-fold-content'>
            <h3>Who We Are</h3>
            <p>

            The Aggie Marine Robotics Robosub Team (AMR) is a
            multidisciplinary team of engineering students from
            Utah State University composed of mechanical,
            electrical, and computer engineers. 
            </p>

            <h3>Our Mission</h3>
            <p>
            To design and build an autonomous underwater
            vehicle (AUV) capable of a variety of tasks. Each year
            we participate in the international AUVSI RoboSub
            competition, sponsored by the office of naval research in
            San Diego, California. The competition is comprised of
            tasks that emulate realistic challenges that civilian and
            military AUVs are expected to perform. Our submarine
            must be capable of identifying its environment and
            interacting with the obstacle course without any human
            intervention.
            </p>
          </BloomerContent>
        </Container>
      </Content>
    </FillParent>
    <Content>
      <Container>
        <BloomerContent styleName='below-fold-content'>
          <h3>The Competition</h3>
          <p>
            The AUVSI RoboSub competition is an 
            international competition held every Summer 
            in San Diego. To learn more about the 
            competition check out the official <a href='https://www.robonation.org/competition/robosub'>RoboSub Website</a>.
          </p>
        </BloomerContent>
      </Container>
    </Content>
  </Fragment>

export default HomePage