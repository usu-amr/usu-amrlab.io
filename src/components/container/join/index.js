import { Fragment } from 'react'
import './index.scss'
import { 
  FillParent, Hero, Content, Footer
} from '../../presentational/basic-page'
import { 
  Content as BloomerContent, Section, Button, Container, 
  Field, Label, Control, FieldLabel, FieldBody, Input, Icon
} from 'bloomer'

const Join = () => 
  <FillParent>
    <Content>
      <Container>
        <BloomerContent styleName='content'>
          <h2>Join the Team</h2>
          <p>If you would like to learn more about our team and how to join message us on our <a href="https://www.facebook.com/AggieMarineRoboticsTeam/">Facebook Page</a>, email us at <a href="mailto:aggiemarinerobotics@gmail.com">aggiemarinerobotics@gmail.com</a>, or drop by our lab on campus at SER 140.</p>
          <h2>Why you should join</h2>
          <p>Work with computer vision, signal analysis, control systems, and autonomous control. Design electrical circuits, power systems, and signal filters.  Engineer submarine components, 3d print, CNC mill, laser cut. Use the skills you are learning in class. Get job experience.</p>
        </BloomerContent>
      </Container>
    </Content>
  </FillParent>

export default Join