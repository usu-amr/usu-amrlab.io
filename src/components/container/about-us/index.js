import { Fragment } from 'react'
import './index.scss'
import { 
  FillParent, Hero, Content, Footer
} from '../../presentational/basic-page'
import { Content as BloomerContent } from 'bloomer'

const AboutUs = () => 
  <FillParent forceFill={true} minHeight='100%'>
    <Fragment>
      <Hero grow={true} styleName='hero'>
        <img 
          src="https://dummyimage.com/1920x1080/009ddc/eee"
          styleName='hero-img'
        />
      </Hero>
      <Content>
        <BloomerContent styleName='content'>
          <h1>About Us</h1>
          <p>
          The Aggie Marine Robotics Robosub Team (AMR) is a
          multidisciplinary team of engineering students from
          Utah State University composed of mechanical,
          electrical, and computer engineers. The goal of our team
          is to design and build an autonomous underwater
          vehicle (AUV) capable of a variety of tasks. Each year
          we participate in the international AUVSI RoboSub
          competition, sponsored by the office of naval research in
          San Diego, California. The competition is comprised of
          tasks that emulate realistic challenges that civilian and
          military AUVs are expected to perform. Our submarine
          must be capable of identifying its environment and
          interacting with the obstacle course without any human
          intervention.
          </p>
        </BloomerContent>
      </Content>
    </Fragment>
  </FillParent>

export default AboutUs