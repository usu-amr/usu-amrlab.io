import { Fragment } from 'react'
import './index.scss'
import { 
  FillParent, Hero, Content, Footer
} from '../../presentational/basic-page'
import { 
  Content as BloomerContent, Section, Button, Container, 
  Field, Label, Control, FieldLabel, FieldBody, Input, Icon
} from 'bloomer'

const MeetTheTeam = () => 
  <FillParent>
    <Content>
      <Container>
        <BloomerContent styleName='content'>
          <h2>Aggie Marine Robotics Team 2018 - 2019</h2>
          <img 
            src='assets\bCzF-ngw.jpeg' 
            styleName='team-image'
          />
          <p>Dr. Jonathon Phillips, Bryant Casutt, Konnor Andrews, Taylor Jones, Reed Thurber, Jacie Grow, Nathan Copier</p>
          
          <h2>2018 - 2019 Team Leads</h2>
          <p>
            <strong>Rachel Wall</strong> - Team lead
          </p>
          <p>
            <strong>Reed Thurber</strong> - Vice lead
          </p>
          <p>
            <strong>Jacie Grow</strong> - Mechanical Captain
          </p>
          <p>
            <strong>Bryant Casutt</strong> - Electrical Captain
          </p>
          <p>
            <strong>Taylor Jones</strong> - Software Captain
          </p>
        </BloomerContent>
      </Container>
    </Content>
  </FillParent>

export default MeetTheTeam