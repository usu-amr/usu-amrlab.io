import { 
  Page, Header, Scroll, Footer, FillParent 
} from './components/presentational/basic-page'
import loadable from '@loadable/component'
import { HashRouter, Route } from 'react-router-dom'

import DevWarning from './components/presentational/dev-warning'
import Nav from './components/container/nav'
import './app.scss'

// import AboutUs from './components/container/about-us'
// import Auvs from './components/container/auvs'
// import Homepage from './components/container/homepage'
// import MeetTheTeam from './components/container/meet-the-team'
// import Join from './components/container/join'

const AboutUs = loadable(
  () => import(
    /* webpackChunkName: "about-us" */
    /* webpackPrefetch: true */
    './components/container/about-us'
  )
)

const Auvs = loadable(
  () => import(
    /* webpackChunkName: "auvs" */
    /* webpackPrefetch: true */
    './components/container/auvs'
  )
)

const Homepage = loadable(
  () => import(
    /* webpackChunkName: "homepage" */
    /* webpackLoad: true */
    './components/container/homepage'
  )
)

const MeetTheTeam = loadable(
  () => import(
    /* webpackChunkName: "meet-the-team" */
    /* webpackPrefetch: true */
    './components/container/meet-the-team'
  )
)

const Join = loadable(
  () => import(
    /* webpackChunkName: "join" */
    /* webpackPrefetch: true */
    './components/container/join'
  )
)

const Sponsor = loadable(
  () => import(
    /* webpackChunkName: "sponsor" */
    /* webpackPrefetch: true */
    './components/container/sponsor'
  )
)

const Sponsors = loadable(
  () => import(
    /* webpackChunkName: "sponsors" */
    /* webpackPrefetch: true */
    './components/container/sponsors'
  )
)

const ContactUs = loadable(
  () => import(
    /* webpackChunkName: "contact-us" */
    /* webpackPrefetch: true */
    './components/container/contact-us'
  )
)

const fillerPage = (content) => 
  <FillParent><p>This page was intentionally left blank</p></FillParent>

const App = () =>
  <HashRouter>
    <Page>
        <Header>
          {/* <DevWarning/> */}
          <Nav/>
        </Header>
        <Scroll>
          <Route path="/" exact component={() => <Homepage/>} />
          <Route path="/team" component={() => <MeetTheTeam/>} />
          <Route path="/submarines" component={() => <Auvs/>} />
          <Route path="/join" component={() => <Join/>} />
          <Route path="/contact" component={() => <ContactUs />} />
          <Route path="/sponsors" component={() => <Sponsors/>} />
          <Route path="/sponsor" component={() => <Sponsor/>} />
          <Footer styleName='footer'>
            Aggie Marine Robotics &copy; 2019
          </Footer>
        </Scroll>
    </Page>
  </HashRouter>

export default App
